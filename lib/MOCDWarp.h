#ifndef MOCDWARP_H
#define MOCDWARP_H

#include "VectorFieldTransform.h"

#include <itkGradientRecursiveGaussianImageFilter.h>
#include "itkGradientImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkMinimumMaximumImageFilter.h"
#include "itkDiscreteGaussianImageFilter.h"
#include "itkSmoothingRecursiveGaussianImageFilter.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkVectorLinearInterpolateImageFunction.h"
#include "itkSubtractImageFilter.h"
#include "itkResampleImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include <itkMinimumMaximumImageCalculator.h>
#include "itkAbsImageFilter.h"

#include <vector>
#include <iostream>
#include <sstream>

#ifndef VERBOSE
#define VERBOSE
#endif

//#ifndef DEBUG
//#define DEBUG
//#endif

template<typename TImage>
class MOCDWarp{
   private:
     
    typedef TImage Image;

    typedef typename Image::Pointer ImagePointer;
    typedef typename Image::RegionType ImageRegion;
    typedef typename Image::SizeType ImageSize;
    typedef typename Image::SpacingType ImageSpacing;
    typedef typename Image::PixelType TPrecision;
    typedef typename Image::IndexType ImageIndex;



    //typedef typename itk::GradientImageFilter<Image, TPrecision, TPrecision>
    typedef itk::CovariantVector< TPrecision, 2  >      OutputPixelType;
    typedef itk::Image< OutputPixelType, 2 >    OutputImageType;
    typedef typename itk::GradientRecursiveGaussianImageFilter<Image, OutputImageType>
      GradientImageFilter;
    typedef typename GradientImageFilter::Pointer GradientImageFilterPointer;

    typedef typename GradientImageFilter::OutputImageType GradientImage;
    typedef typename GradientImage::Pointer GradientImagePointer;
    typedef typename GradientImage::PixelType GradientType;

    typedef typename itk::ImageRegionIterator<Image> ImageIterator;
    typedef typename itk::ImageRegionIteratorWithIndex<Image> IndexedImageIterator;
    

    typedef typename itk::ImageRegionIterator<GradientImage> GradientImageIterator;

    typedef typename itk::VectorLinearInterpolateImageFunction<GradientImage, TPrecision>
      GradientLinearInterpolate;
    typedef typename GradientLinearInterpolate::Pointer GradientLinearInterpolatePointer;
    typedef typename GradientLinearInterpolate::OutputType
      GradientInterpolateOutput;

    
    typedef typename itk::SmoothingRecursiveGaussianImageFilter<Image, Image> GaussianFilter;
    typedef typename GaussianFilter::Pointer GaussianFilterPointer;
    
    typedef typename itk::MultiplyImageFilter<Image, Image> MultiplyFilter;
    typedef typename MultiplyFilter::Pointer MultiplyFilterPointer;
    
    typedef typename itk::SubtractImageFilter<Image, Image> SubtractFilter;
    typedef typename SubtractFilter::Pointer SubtractFilterPointer;


    typedef typename itk::ResampleImageFilter<Image, Image> ResampleFilter;
    typedef typename ResampleFilter::Pointer ResampleFilterPointer;

    typedef typename itk::RescaleIntensityImageFilter<Image, Image> RescaleFilter;
    typedef typename RescaleFilter::Pointer RescaleFilterPointer;

    typedef typename itk::AbsImageFilter<Image, Image> AbsFilter;
    typedef typename AbsFilter::Pointer AbsFilterPointer;

    typedef typename itk::MinimumMaximumImageCalculator<Image> MinMaxCalculator;
    typedef typename MinMaxCalculator::Pointer MinMaxCalculatorPointer;



    TPrecision diffTolerance;
    int nIterations;
    int nTimeSteps;
    TPrecision dt;
    TPrecision diffusionWeight;
    TPrecision gamma;
    TPrecision maxMotion;
    TPrecision diffEnd;
    TPrecision gStep;

    //Debug var
    int currentScale;
    int currentIter;

  public:
    
    typedef typename itk::LinearInterpolateImageFunction<Image, TPrecision>
      LinearInterpolate;
    typedef typename LinearInterpolate::Pointer LinearInterpolatePointer;
    typedef typename LinearInterpolate::ContinuousIndexType ImageContinuousIndex;

    typedef VectorFieldTransform<Image, TPrecision, LinearInterpolate>
      ImageTransform;

    typedef typename ImageTransform::VImage VImage;
    typedef typename VImage::VectorType VectorType;



    MOCDWarp(){
      gamma = 20;
      diffTolerance = 1e-6;
      diffusionWeight = 0.5;
      nTimeSteps = 20;
      nIterations = 200;
      dt = 1.0/nTimeSteps;
      gStep = 0.5;
      currentScale=0;
    };

 
    //Do a multires warp starting with nres resolutions
    void warpMultiresolution(ImagePointer i1, ImagePointer i2, int nres){
      
      RescaleFilterPointer rescale = RescaleFilter::New();
      rescale->SetInput(i1);
      rescale->SetOutputMaximum(10.0);
      rescale->SetOutputMinimum(0.0);
      rescale->Update();
      i1 = rescale->GetOutput();
      
      rescale = RescaleFilter::New();
      rescale->SetInput(i2);
      rescale->SetOutputMaximum(10.0);
      rescale->SetOutputMinimum(0.0);
      rescale->Update();
      i2 = rescale->GetOutput();

#ifdef VERBOSE
      std::cout << "Computing Multires pyramid" << std::endl;
#endif

      ImagePointer *pyramid1 = downsample(i1, nres, 2);
      ImagePointer *pyramid2 = downsample(i2, nres, 2);

#ifdef VERBOSE
      std::cout << "Computing Multires pyramid - done" << std::endl;
#endif


      std::vector<VImage *> &maps;
      std::vector<VImage *> &velocities;

      //TPrecision maxMotionTmp = maxMotion;
      //Multires steps
      for(int i=nres-1; i>0; i--){
        //do warp      
	currentScale = i;

#ifdef VERBOSE
        std::cout << "Warp at scale " << i << std::endl;
#endif


        warp(pyramid1[i], pyramid2[i], maps, velocities);

        upsample( maps, pyramid1[i-1]->GetLargestPossibleRegion().GetSize(), pyramid1[i-1]->GetSpacing() );
	recomputeVelocities(maps, velocities)
      }

#ifdef VERBOSE
      std::cout << "Warp at scale " << 0 << std::endl;
#endif
      currentScale = 0;
      warp(pyramid1[0], pyramid2[0], velocities);
      
      delete[] pyramid1;
      delete[] pyramid2;
    };




    //Compute vectorfield v minimizing  
    // Int ( i1(x + v(x)) - i2 ) dx + alpha * Int | grad v(x) |^2 dx  
    void warp( ImagePointer i1, ImagePointer i2, 
	       std::vector< VImage * > &maps, 
	       std::vector< VImage * > &velocities){
//#ifdef DEBUG
	    {
	 std::stringstream outfile;
	 outfile << "start_moving_scale_" << currentScale << ".nrrd";
	 ImageIO<Image>::saveImage( i1, outfile.str()  );
	    }
	    {
	 std::stringstream outfile;
	 outfile << "moved_scale_" << currentScale << "_iter_" << nIterations << ".nrrd";
	 ImageIO<Image>::saveImage( i2, outfile.str()  );
	    }
//#ifdef DEBUG
      static TPrecision EPSILON = 1.0e-20;
      const int dimension = i1->GetImageDimension();

      //Transformation vector field (v)
      if(velocities.size() != nTimeSteps+1 ){
	velocities.resize( nTimeSteps+1 );
        for(int i=0; i< velocities.size(); i++){
	  if(velocities[i] != NULL ){
	    delete velocities[i];
	  }
          velocities[i] = ImageTransform::InitializeZeroTransform(i1);
        }
      }
       
      if(maps.size() != nTimeSteps ){
	maps.resize( nTimeSteps );
        for(int i=0; i< maps.size(); i++){
	  if(maps[i] != NULL ){
	    delete maps[i];
	  }
          maps[i] = ImageTransform::InitializeZeroTransform(i1);
        }
      }
      std::vector< ImagePointer > imflow( nTimeSteps+1 );





      //Iterate until maximum number of iterations or 
      //stopping criteria is achieved   
      int nIter = 0;     
      TPrecision prevSumDiffIntensity = std::numeric_limits<TPrecision>::max();

      
      std::vector< ImagePointer > imflow( nTimeSteps+1 );
      std::vector< ImagePointer > lambda( nTimeSteps+1 );
      for(int i=0; i <= nTimeSteps; i++){
        lambda[i] = NULL;
        imflow[i] = ImageIO< Image >::copyImage( i1 );
      }

     imflow[0] = ImageIO< Image >::copyImage( i1 );

      std::vector< GradientImagePointer > gimflow( nTimeSteps + 1 );
      {
	 gimflow[0] = getGradient( imflow[0] );
      }

      
      for(;nIter<nIterations; nIter++){
	 
	 currentIter = nIter;

#ifdef VERBOSE
         std::cout << "iteration: " << nIter << std::endl;
#endif
	 mapForwardFlow( velocities, maps, gmaps );

	 mapBackwardFlow( i2, imflow.back(), velocities, lambda );

         updateVelocities( velocities, lambda, imflow, gimflow );
	 

//#ifdef DEBUG
	 std::stringstream outfile;
	 outfile << "moved_scale_" << currentScale << "_iter_" << nIter << ".nrrd";
	 ImageIO<Image>::saveImage( imflow.back(), outfile.str()  );
//#ifdef DEBUG

      }

//#ifdef DEBUG
      for(int i=0; i<imflow.size(); i++){
	 std::stringstream outfile;
	 outfile << "flow_scale_" << currentScale << "_ts_" << i << ".nrrd";
	 ImageIO<Image>::saveImage( imflow[i], outfile.str()  );
      }
//#ifdef DEBUG

#ifdef VERBOSE
      std::cout << "nIterations: " << nIter << std::endl;
#endif

    };


    //Diffusionweight, smoothness of vectorfield
    void setDiffusionWeight(TPrecision diffw){
      diffusionWeight = diffw;
    };
    
    TPrecision getDiffusionWeight(){
      return diffusionWeight;
    };

    //mx number of iterations
    void setMaximumIterations(int maxIter){
      nIterations = maxIter;
    };

    //Stopping criterium: average pixel intensity difference < difference tolerance 
    void setDifferenceTolerance(TPrecision tol){
      diffTolerance = tol;
    }; 
    
    TPrecision getDifferenceTolerance(){
      return diffTolerance;
    };

    void setNumberOfTimeSteps(int nt){
      nTimeSteps = nt;
      dt = 1.0/nTimeSteps;
    }; 
    
    TPrecision getNumberOfTimeSteps(){
      return nTimeSteps;
    };

    void setGamma(TPrecision g){
      gamma= g;
    };

    void setGradientStepSize(TPrecision step){
      gStep = step;
    };

    TPrecision getResultDifference(){
      return diffEnd;
    };

    //motion scaling per iteratiitk multiplybyconstanton
    void setMaximumMotion(TPrecision maxm){
      maxMotion = maxm;
    };




  
  private:

    MinMaxCalculatorPointer getMinMax(ImagePointer image){
         //AbsFilterPointer absFilter = AbsFilter::New();
	 //absFilter->SetInput( image );
	 //absFilter->Update();
	 MinMaxCalculatorPointer mmc = MinMaxCalculator::New();
	 //mmc->SetImage( absFilter->GetOutput() );
	 mmc->SetImage( image );
	 mmc->Compute();
	 return mmc;
    };

    GradientImagePointer getGradient(ImagePointer im){
	 ImageSpacing spacing = im->GetSpacing();
	 MultiplyFilterPointer mul = MultiplyFilter::New();
	 mul->SetInput(im);
	 mul->SetConstant( spacing[0] );
	 GradientImageFilterPointer gradFilter = GradientImageFilter::New();
         //gradFilter->SetUseImageSpacing(false);
	 gradFilter->SetSigma( 2.5 * spacing[0] );
         gradFilter->SetUseImageDirection(false);
	 gradFilter->SetInput( mul->GetOutput() );
	 gradFilter->Update();
	 return gradFilter->GetOutput();
    };

    void  imageForwardFlow( std::vector<VImage *> &velocities, 
		            std::vector<ImagePointer> &imflow, 
			    std::vector<GradientImagePointer> &gimflow){
       
      const int dimension = Image::ImageDimension;
      ImageRegion region = imflow[0]->GetLargestPossibleRegion();
      itk::Vector<TPrecision, dimension> vTmp;
      
      for(int i=1; i<imflow.size(); i++){
         
         ImageIterator i1It(imflow[i-1], region);
         ImageIterator i2It(imflow[i], region);
	       
	 GradientImagePointer grad = gimflow[i-1];
         GradientImageIterator gradIt( grad, region );
	 
	 VImage *v = velocities[i-1];
	 
	 v->initIteration(region);
	 gradIt.GoToBegin();
	 i1It.GoToBegin();
	 i2It.GoToBegin();
	 
	 TPrecision maxStep = 0;
	 for(; !gradIt.IsAtEnd(); ++gradIt, ++i1It, ++i2It, v->next()){
	    v->get( vTmp );
	    TPrecision step =  dt * ( gradIt.Get() * vTmp );
	    i2It.Set( i1It.Get() - step );
	    maxStep = std::max( maxStep, std::fabs(step) );
	 }
	 std::cout << "Max step forward: " << maxStep << std::endl;
           
	 gimflow[i] = getGradient( imflow[i]  ); 
       }

    };
    


    void adjointBackwardFlow(ImagePointer i2, ImagePointer forward, 
		             std::vector<VImage *> &velocities, 
			     std::vector<ImagePointer> &lambda){
    
      const int dimension = Image::ImageDimension;
      ImageRegion region = forward->GetLargestPossibleRegion();
      itk::Vector<TPrecision, dimension> vTmp;
      
      SubtractFilterPointer subtractFilter = SubtractFilter::New();
      subtractFilter->SetInput1( i2 );
      subtractFilter->SetInput2( forward );
      subtractFilter->Update();
      
#ifdef DEBUG
      {
        std::stringstream outfile;
        outfile << "diff_scale_" << currentScale << "_iter_" << currentIter <<".nrrd";
        ImageIO<Image>::saveImage( subtractFilter->GetOutput(), outfile.str() );
      }
#endif

      MinMaxCalculatorPointer mima = getMinMax( subtractFilter->GetOutput() );
      TPrecision scaling = std::max( fabs( mima->GetMaximum() ), fabs( mima->GetMinimum() ) );

      MultiplyFilterPointer multiplyFilter = MultiplyFilter::New();
      multiplyFilter->SetInput( subtractFilter->GetOutput() );
      multiplyFilter->SetConstant( gamma/scaling );
      multiplyFilter->GetOutput();

      lambda[nTimeSteps] = multiplyFilter->GetOutput();

#ifdef DEBUG
      {
        std::stringstream outfile;
        outfile << "lambda_scale_" << currentScale << "_iter_" << currentIter << "_ts_" << nTimeSteps <<".nrrd";
        ImageIO<Image>::saveImage( lambda[nTimeSteps], outfile.str() );
      }
#endif

      for(int i=nTimeSteps; i>0; i--){
	 
	 VImage *v = velocities[i];
	 
	 ImagePointer div = v->weightedDivergence( lambda[i] );

 
	 MultiplyFilterPointer multiplyFilter = MultiplyFilter::New();
         multiplyFilter->SetInput( div );
         multiplyFilter->SetConstant( -dt );
         div = multiplyFilter->GetOutput();

	 SubtractFilterPointer subtractFilter = SubtractFilter::New();
         subtractFilter->SetInput1( lambda[i] );
         subtractFilter->SetInput2( div );
	 subtractFilter->Update();
         lambda[i-1] = subtractFilter->GetOutput();
         

	 //Debug 
	 MinMaxCalculatorPointer mmc = getMinMax(div);
	 Precision maxStep = mmc->GetMaximum();
	 Precision minStep = mmc->GetMinimum();
	 std::cout << "Max step backwards: " << maxStep << " , " << minStep << std::endl;

#ifdef DEBUG
	 std::stringstream outfile;
	 outfile << "lambda_scale_" << currentScale << "_iter_" << currentIter << "_ts_" << i-1 <<".nrrd";
	 ImageIO<Image>::saveImage( lambda[i-1], outfile.str() );
#endif

      }

    };



    void updateVelocities( std::vector<VImage *> &velocities, 
		           std::vector<ImagePointer> &lambda, 
			   std::vector<ImagePointer> &imflow,
			   std::vector<GradientImagePointer> &gimflow){
   

	 for(int i=0; i<velocities.size(); i++){
	   VImage *v = new VImage( gimflow[i] );
	   v->multiply( lambda[i] );
	   v->blur( diffusionWeight );
	   v->multiply( -gStep );
	   velocities[i]->multiply( 1 - 2*gStep );
	   velocities[i]->add(v);

	 
#ifdef DEBUG
	   //Debug
	   ImagePointer mv = velocities[i]->magnitudeSquared();
	   std::stringstream outfile;
	   outfile << "mv_scale_" << currentScale << "_iter_" << currentIter << "_ts_" << i <<".nrrd";
	   ImageIO<Image>::saveImage( mv, outfile.str() );
#endif

	   delete v;
	 }
	 
    };



    //Create multiresolution images
    ImagePointer *downsample(ImagePointer im, int nres, TPrecision sigma){
      ImagePointer *pyramid = new ImagePointer[nres+1];
      pyramid[0] = im; 
      //Downsample
      for(int i=1; i<nres; i++){
        ImageSize size = pyramid[i-1]->GetLargestPossibleRegion().GetSize();
        ImageSpacing spacing = pyramid[i-1]->GetSpacing();
        
	GaussianFilterPointer smooth = GaussianFilter::New();
	//TODO deal with anistropic voxels
        smooth->SetSigma(sigma * spacing[0]);
        smooth->SetInput(pyramid[i-1]);
        smooth->Update();

        ResampleFilterPointer downsample = ResampleFilter::New();
        downsample->SetInput(smooth->GetOutput());
        downsample->SetOutputParametersFromImage(pyramid[i-1]);
                
        for(unsigned int n = 0; n < size.GetSizeDimension(); n++){
          size[n] = size[n]/2;
          spacing[n] = spacing[n]*2;
        }
          
        downsample->SetOutputSpacing(spacing); 
        downsample->SetSize(size);

        downsample->Update();
        pyramid[i] = downsample->GetOutput();       
      }

      return pyramid;
    };



    void upsample(std::vector<VImage *> &velocities, ImageSize size, ImageSpacing spacing){
      typedef typename VImage::ITKVectorImage ITKVectorImage;
      //upsample transfrom
      for(int i=0; i<velocities.size(); i++){
        {
	   std::stringstream outfile;
	   outfile << "v_scale_" << currentScale << "_ts_" << i <<".nrrd";
	   ImageIO< Image >::saveImage( velocities[i]->magnitudeSquared(), outfile.str() );
        }

        upsample(velocities[i], size, spacing);
        
	{
	   std::stringstream outfile;
	   outfile << "v_scale_" << currentScale << "_ts_" << i <<"_up.nrrd";
	   ImageIO< Image >::saveImage( velocities[i]->magnitudeSquared(), outfile.str() );
        }
      }

    };
    
    void upsample(VImage *transform, ImageSize size, ImageSpacing spacing){
      //upsample transfrom
      ImagePointer *vfieldComps = transform->getComps();
      for(unsigned int j=0; j <  Image::GetImageDimension(); j++){
        ResampleFilterPointer upsample = ResampleFilter::New();
        upsample->SetInput(vfieldComps[j]);
        upsample->SetOutputParametersFromImage(vfieldComps[j]);
                
        //ImageSize size = vfieldComps[j]->GetLargestPossibleRegion().GetSize();
        //ImageSpacing spacing = vfieldComps[j]->GetSpacing();  
          
        //for(unsigned int n = 0; n < size.GetSizeDimension(); n++){
        //  size[n] = size[n]*2;
        //  spacing[n] = spacing[n] / 2;
       // }
        
        upsample->SetOutputSpacing(spacing); 
        upsample->SetSize(size);
        

        upsample->Update();	 
	
	MultiplyFilterPointer multiplyFilter = MultiplyFilter::New();
        multiplyFilter->SetInput( upsample->GetOutput() );
        multiplyFilter->SetConstant( 2.0 );
        multiplyFilter->Update();
        vfieldComps[j] = multiplyFilter->GetOutput();
	//GaussianFilterPointer blur = GaussianFilter::New();
	//blur->SetSigma( 2.0 * spacing[0] );
	//blur->SetInput( upsample->GetOutput() );
	//blur->Update();
        //vfieldComps[j] = blur->GetOutput();

        //vfieldComps[j] = upsample->GetOutput();
        transform->setComp(j, vfieldComps[j]);
      }

    };
};


#endif
