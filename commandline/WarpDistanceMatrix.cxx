#define PRECISION
typedef float Precision;

#include "MyConfig.h"

#include "DenseMatrix.h"
#include "ImageIO.h"
#include "IO.h"
#include "LinalgIO.h"
#include "VectorImage.h"
#include "itkImageRegionIterator.h"
#include "ElasticWarp.h"

typedef Image::Pointer ImagePointer;
typedef SimpleWarp<Image> Warp;
typedef Warp::VImage VImage;
typedef VImage::ITKVectorImage ITKVectorImage;
typedef itk::ImageRegionIterator<Image> ImageIterator;
typedef Warp::ImageTransform ImageTransform;


int main(int argc, char **argv){
  using namespace FortranLinalg;

  if(argc < 3 ){
    std::cout << "Usage:" << std::endl;
    std::cout << argv[0] << " imagelist outfile";
    std::cout << std::endl;
    return 0;
  }

 itk::MultiThreader::SetGlobalMaximumNumberOfThreads(1); 
 
   
  int argIndex = 1;
 
  //read list of images that were pairwise registered
  //assumes that in the current directory is an vector image 
  //warp_i_j.nrrd to read transform from 
  std::vector<std::string> imagesList =
    IO<Precision>::readStringList(argv[argIndex++]);
  int N = imagesList.size();
  
  std::vector<std::string> images(N);
  std::vector<std::string>::iterator fileIt = imagesList.begin();
  for(int i=0; i < N; ++i, ++fileIt){
    images[i] = *fileIt;
  }
  char *outfile = argv[argIndex++];

  DenseMatrix<double> jf(N, N);
  DenseMatrix<double> mag(N, N);
  DenseMatrix<double> intensity(N, N);


  for(int i=0; i< N; i++){
    std::cout << i << " ";
    for(int j=0; j < N; j++){
      std::stringstream ss;
      ss << "warp_" << i << "_" << j << ".nrrd";
      //std::cout << ss.str() << std::endl;
      if( IO<int>::fileExists( ss.str() ) ){
        ITKVectorImage::Pointer warp = ImageIO<ITKVectorImage>::readImage( ss.str() );
        VImage v(warp);
        jf(i, j) = v.sumJacobianFrobeniusSquared();
        mag(i, j) = v.sumMagnitudeSquared();
        ImagePointer image2 = ImageIO<Image>::readImage(images[j]);
        ImagePointer warped = ImageTransform::Transform( image2, &v);

        double dist = 0;
        double tmp;
        ImagePointer image = ImageIO<Image>::readImage(images[i]);
        ImageIterator it1( image, image->GetLargestPossibleRegion());
        ImageIterator it2( warped, warped->GetLargestPossibleRegion());
        for(;!it1.IsAtEnd(); ++it1, ++it2){
         tmp = it1.Get() - it2.Get();
         dist += tmp*tmp;
        }
        intensity(i, j) = dist;

      }
      else{
        jf(i, j) = -1;
        mag(i, j) = -1;
        intensity(i, j) = -1;
      } 
    } 
  }
  std::cout << std::endl;


  std::stringstream ss1;
  ss1 << outfile <<"_jf.data";
  LinalgIO<double>::writeMatrix(ss1.str(), jf);

  std::stringstream ss2;
  ss2 << outfile <<"_mag.data";
  LinalgIO<double>::writeMatrix(ss2.str(), mag);

  std::stringstream ss3;
  ss3 << outfile <<"_intensity.data";
  LinalgIO<double>::writeMatrix(ss3.str(), intensity);


  return 0;

}
