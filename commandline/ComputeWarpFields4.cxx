#define PRECISION
typedef float Precision;

#include "MyConfig.h"


#include "ImageIO.h"
#include "IO.h"
#include "LinalgIO.h"
#include "Linalg.h"

#include <tclap/CmdLine.h>

//shared memory, process stuff
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>   
#include <sys/wait.h>


int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("Compute elastic image warps for selected image pairs", ' ', "1");

  TCLAP::ValueArg<std::string> ind2Arg("","index2",
      "Image id 2 .", 
      true, "", "filename");
  cmd.add(ind2Arg);

  TCLAP::ValueArg<std::string> ind1Arg("","index1",
      "Image id 1.", 
      true, "", "filename");
  cmd.add(ind1Arg);
  
  TCLAP::ValueArg<int> nprocArg("p","nproc","number of processors to use", true, 1, "int");
  cmd.add(nprocArg);

  TCLAP::ValueArg<std::string> maskArg("m","mask","mask image", false, "",
      "filename");
  cmd.add(maskArg);

  TCLAP::ValueArg<Precision> stepArg("s","scaling",
      "scaling of maximum step size for gradient descent", false, (Precision)0.2, 
      "step size");
  cmd.add(stepArg);

  TCLAP::ValueArg<int> iterArg("i","iterations",
      "maximum number of iterations per scale", false, 200, 
      "int");
  cmd.add(iterArg);

  TCLAP::ValueArg<Precision> alphaArg("","alpha",
      "weight of gradient (alpha) and identity (1-alpha) penalty", false, (Precision) 0.5, 
      "float");
  cmd.add(alphaArg);
  

  TCLAP::ValueArg<Precision> lambdaArg("","lambda",
      "inital weight of image differnce term", false, (Precision) 1, 
      "float");
  cmd.add(lambdaArg);

  TCLAP::ValueArg<Precision> lambdaIncArg("","lambdaInc",
      "increase in lambda when iamges can not be registered within epsilon for inital given lambda ", 
      false, (Precision) 0.1, 
      "float");
  cmd.add(lambdaIncArg);
  
  TCLAP::ValueArg<Precision> lambdaIncTArg("","lambdaIncThreshold",
      "Threshold in intensity difference between two consecutive updates to update lambda", 
      false, (Precision) 0.0001, 
      "float");
  cmd.add(lambdaIncTArg);

  TCLAP::ValueArg<Precision> epsArg("","epsilon",
      "Maximal root mean squared intensity difference to register images to", false, (Precision) 0.01, 
      "tolerance");
  cmd.add(epsArg);

  TCLAP::ValueArg<Precision> sigmaArg("","sigma",
      "Multiscale sigma of coarsest scale. Sigma is halfed at each subsequent scale"
      , false, (Precision) 8, "float");
  cmd.add(sigmaArg);

  TCLAP::ValueArg<int> nscaleArg("","nscales",
      "Number of scales. 0 corresponds to no scales", false, 5, 
      "int");
  cmd.add(nscaleArg);  
  
  TCLAP::SwitchArg mresArg("", "useMultires", 
      "do multiresultion instead of  multiscale"); 
  cmd.add(mresArg);
 
  TCLAP::SwitchArg niceArg("", "nice", 
      ""); 
  cmd.add(niceArg);

  try{
	  cmd.parse( argc, argv );
	} 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }

   

  try{

  std::vector<std::string> ind1List =
    IO<Precision>::readStringList(ind1Arg.getValue());
  int N = ind1List.size();
  
  std::vector< std::string > index1(N);
  std::vector<std::string>::iterator file1It = ind1List.begin();
  for(int i=0; i < N; ++i, ++file1It){
    index1[i] = *file1It;
  }
 
  std::vector<std::string> ind2List =
    IO<Precision>::readStringList(ind2Arg.getValue());
  
  std::vector< std::string > index2(N);
  std::vector<std::string>::iterator file2It = ind2List.begin();
  for(int i=0; i < N; ++i, ++file2It){
    index2[i] = *file2It;
  }
  
  std::cout << N << std::endl;

  std::string maskImage = maskArg.getValue();
  std::string prefix = "/usr/sci/xom/BrainAll/DataProcessed_RandomPairs/brain_regTo4_";

  int nProcInUse = 0;
  int nProcessors = nprocArg.getValue();
  for(int i=0; i< N; i++){
    std::stringstream image1;
    std::stringstream image2;
    image1 << prefix << index1[i] << ".nii.gz";
    image2 << prefix << index2[i] << ".nii.gz";
    for(int j=0; j < 2; j++){

        pid_t pid;
        if(nProcInUse < nProcessors){
          pid = fork();
          nProcInUse++;
        }
        else{
          int status;
          wait(&status);
          pid = fork();
        }
        //Child computes one distance
        if(pid == 0){
          std::stringstream ss;
          if(niceArg.getValue()){
            ss << "nice ";
          }        
          ss << "ElasticRegistration_" << DIMENSION << "D "; 
	        if(j == 0){
            ss << "-f " << image1.str() << " ";
            ss << "-m " << image2.str() << " ";
	        }else{
            ss << "-f " << image2.str() << " ";
            ss << "-m " << image1.str() << " ";
	        }
          if(maskImage.length() > 0){
            ss << "--mask "  << maskArg.getValue() << " ";
          }
          ss << "-d warp_" << i << "_" << j <<".nrrd ";
          //ss << "--info warp_" << i << "_" << j << ".info ";
          ss << "-s " << stepArg.getValue() << " ";
          ss << "-i " << iterArg.getValue() << " ";
          ss << "--alpha " << alphaArg.getValue() << " ";
          ss << "--epsilon " << epsArg.getValue() << " "; 
          ss << "--lambda " << lambdaArg.getValue() << " ";
          ss << "--lambdaInc " << lambdaIncArg.getValue() << " ";
          ss << "--lambdaIncThreshold " << lambdaIncTArg.getValue() << " ";
          ss << "--sigma " << sigmaArg.getValue() << " ";
          ss << "--nscales " << nscaleArg.getValue() << " ";
          if(mresArg.getValue()){
          ss << "--useMultires ";
          }
          std::cout << ss.str() << std::endl;
          std::system(ss.str().c_str());
          return 0;
          
        }
    } 
  }

  while(nProcInUse > 0){
    int status;
    wait(&status);
    nProcInUse--;
  }

  return 0;
  
  }
  catch(char *err){
    std::cerr << err << std::endl;
    return -1;
  }

}
