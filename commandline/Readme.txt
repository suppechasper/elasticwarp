This folder contains the source files to compile the elastic registration and
deformable image kernel regression used in the paper "Manifold Modelling for
Brain Population Analysis". It also contains the code for reading and writing
the pairwise distance matrices available from www.math.duke.edu/~sgerber that
were used in the paper.

Compilataion is setup through CMake and requires ITK.
