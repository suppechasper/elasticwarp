#define PRECISION
typedef float Precision;

#define VERBOSE
#define USE_ORIENTED

#include "MyConfig.h"

#include "OCDWarp.h"
#include "ImageIO.h"

#include "itkCastImageFilter.h"
#include "itkImageRegionIterator.h"

#include <tclap/CmdLine.h>

typedef OCDWarp<Image> Warp; 

typedef Image::Pointer ImagePointer;

int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("Optimal control diffemoprhic registration", ' ', "1");

  TCLAP::ValueArg<std::string> fixedArg("f","fixed","fixed image", true, "",
      "filename");
  cmd.add(fixedArg);

  TCLAP::ValueArg<std::string> movingArg("m","moving","moving image", true, "",
      "filename");
  cmd.add(movingArg);
  
  TCLAP::ValueArg<Precision> stepArg("s","scaling",
      "scaling of maximum step size for gradient descent", false, (Precision)0.2, 
      "float");
  cmd.add(stepArg);  
  
  TCLAP::ValueArg<Precision> gArg("g","gradstep",
      "Gradient step size", false, (Precision)0.2, 
      "float");
  cmd.add(gArg);
  
  TCLAP::ValueArg<Precision> gammaArg("","gamma",
      "Image penalty factor", false, (Precision) 2, 
      "float");
  cmd.add(gammaArg);
  
  TCLAP::ValueArg<Precision> deltaArg("","delta",
      "Minimum update between steps", false, (Precision) 1e-6, 
      "float");
  cmd.add(deltaArg);
  
  TCLAP::ValueArg<Precision> dArg("d","diffusion",
      "Spatial smoothness of velocity field", false, (Precision) 0.5, 
      "float");
  cmd.add(dArg);
  
  TCLAP::ValueArg<int> iterArg("i","iterations",
      "maximum number of iterations per scale", false, 200, 
      "int");
  cmd.add(iterArg);
    
  TCLAP::ValueArg<int> tArg("t","timesteps",
      "Number of time steps", false, 20, 
      "int");
  cmd.add(tArg);
  
  TCLAP::ValueArg<Precision> epsArg("","epsilon",
      "Maximal root mean squared intensity difference to register images to", false, (Precision) 0.01, 
      "tolerance");
  cmd.add(epsArg);

  TCLAP::ValueArg<int> nscaleArg("","nscales",
      "Number of scales. 0 corresponds to no scales", false, 5, 
      "int");
  cmd.add(nscaleArg);

  try{
    cmd.parse( argc, argv );
  } 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }

 itk::MultiThreader::SetGlobalMaximumNumberOfThreads(1); 
 //Input Images
 ImagePointer fixed = ImageIO<Image>::readImage(fixedArg.getValue());
 ImagePointer moving = ImageIO<Image>::readImage(movingArg.getValue());

 //Setup warp
 Warp warp;
 warp.setMaximumIterations( iterArg.getValue() );
 warp.setMaximumMotion( stepArg.getValue() );
 warp.setNumberOfTimeSteps( tArg.getValue() );
 warp.setGamma( gammaArg.getValue() );
 warp.setGradientStepSize( gArg.getValue() );
 warp.setDiffusionWeight( dArg.getValue() );
 warp.setDifferenceTolerance( epsArg.getValue() );
 //warp.setEpsilon(epsArg.getValue());

 //Do warp
 std::vector<Warp::VImage *> velocities;
 
 warp.warpMultiresolution(moving, fixed, 
		          nscaleArg.getValue(), velocities);
 
 
 
 
 return 0;

}

